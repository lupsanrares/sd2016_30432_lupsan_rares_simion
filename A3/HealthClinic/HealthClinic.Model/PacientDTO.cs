﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HealthClinic.Model
{
    public class PacientDTO
    {
        [Required]
        [RegularExpression("[0-9]{13}",ErrorMessage="Invalid PNC format")]
        public string PNC { get; set; }
        [Required]
        [StringLength(255, MinimumLength = 5)]
        public String Name { get; set; }

        [Required]
        [StringLength(8, MinimumLength = 8)]
        [RegularExpression("[a-zA-Z]{2}[0-9]{6}",ErrorMessage="Invalid ICN format")]
        public String ICN { get; set; }

        [Required]
        [StringLength(255)]
        public String Address { get; set; }

        [Required]
        [RegularExpression("[0-9]{10}",ErrorMessage="Invalid phone number")]
        public String Phone { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
    }
}
