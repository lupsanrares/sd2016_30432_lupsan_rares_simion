﻿using AutoMapper;
using HealthClinic.DAL;
using HealthClinic.Model;
using HealthClinic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthClinic.App_Start
{
    public static class InitializeAutoMapper
    {
        public static void Initialize()
        {
            Mapper.CreateMap<AspNetUser, UserDTO>();
            Mapper.CreateMap<UserDTO, AspNetUser>();
            Mapper.CreateMap<EmployeeViewModel, UserDTO>();
            Mapper.CreateMap<UserDTO, EmployeeViewModel>();
            Mapper.CreateMap<Pacient, PacientDTO>();
            Mapper.CreateMap<PacientDTO, Pacient>();
            Mapper.CreateMap<ConsultationViewModel, ConsultationDTO>();
            Mapper.CreateMap<ConsultationDTO, ConsultationViewModel>();
            Mapper.CreateMap<Consultation, ConsultationDTO>();
            Mapper.CreateMap<ConsultationDTO, Consultation>();
        }
    }
}