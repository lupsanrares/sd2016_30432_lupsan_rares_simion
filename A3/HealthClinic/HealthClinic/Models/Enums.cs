﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace HealthClinic.Models
{
    public class Enums
    {
        public enum Hours
        {
            [Description("08:00")]
            Eight,
            [Description("09:00")]
            Nine,
            [Description("10:00")]
            Ten,
            [Description("11:00")]
            Eleven,
            [Description("12:00")]
            Twelve,
            [Description("13:00")]
            Thirteen,
            [Description("14:00")]
            Fourteen,
            [Description("15:00")]
            Fifteen,
            [Description("16:00")]
            Sixteen
        }
    }
}