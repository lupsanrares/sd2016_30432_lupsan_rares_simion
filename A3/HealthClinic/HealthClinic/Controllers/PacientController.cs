﻿using AutoMapper;
using HealthClinic.DAL.UoW;
using HealthClinic.Model;
using HealthClinic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using Microsoft.AspNet.SignalR;
using HealthClinic.Hubs;

namespace HealthClinic.Controllers
{
    public class PacientController : Controller
    {
        IPacientUnitOfWork _PacientUnitOfWork;
        IConsultationUnitOfWork _ConsultationUnitOfWork;
        IUserUnitOfWork _UserUnitOfWork;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        public PacientController(IPacientUnitOfWork PacientUoW,  IConsultationUnitOfWork ConsultationUoW,IUserUnitOfWork _UserUoW)
        {
            _PacientUnitOfWork = PacientUoW;
            _ConsultationUnitOfWork = ConsultationUoW;
            _UserUnitOfWork = _UserUoW;

        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return this._roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set { this._roleManager = value; }
        }
        // GET: Pacient
        [System.Web.Mvc.Authorize(Roles="Admin, Secretary, Doctor")]
        public ActionResult Index()
        {
            return View(_PacientUnitOfWork.GetAll());
        }

        // GET: Pacient/Details/5
        public ActionResult Details(String pnc)
        {
            return View();
        }

        // GET: Pacient/Create
        [System.Web.Mvc.Authorize(Roles = "Admin, Secretary")]
        public ActionResult Create()
        {
            return View();
        }
        [System.Web.Mvc.Authorize(Roles = "Admin, Secretary")]
        // POST: Pacient/Create
        [HttpPost]
        public ActionResult Create(PacientDTO pacient)
        {
            try
            {
                _PacientUnitOfWork.Create(pacient);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [System.Web.Mvc.Authorize(Roles = "Admin, Secretary")]
        // GET: Pacient/Edit/5
        public ActionResult Edit(String pnc)
        {
            return View(_PacientUnitOfWork.GetById(pnc));
        }

        // POST: Pacient/Edit/5
        [System.Web.Mvc.Authorize(Roles = "Admin, Secretary")]
        [HttpPost]
        public ActionResult Edit(PacientDTO pacient)
        {
            try
            {
                _PacientUnitOfWork.Update(pacient);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Pacient/Delete/5
        [System.Web.Mvc.Authorize(Roles = "Admin, Secretary")]
        public ActionResult Delete(String pnc)
        {
            
            return View(_PacientUnitOfWork.GetById(pnc));
        }

        // POST: Pacient/Delete/5
        [System.Web.Mvc.Authorize(Roles = "Admin, Secretary")]
        [HttpPost]
        public ActionResult Delete(PacientDTO pacient)
        {
            try
            {

                _PacientUnitOfWork.Delete(pacient);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Pacient/Delete/5
        [System.Web.Mvc.Authorize(Roles = "Admin, Secretary")]
        public async Task<ActionResult> Schedule(String pnc)
        {
            List<UserDTO> doctors=new List<UserDTO>();
            var users=_UserUnitOfWork.GetAll();
            foreach(var user in users)
            {
                var valid=await UserManager.IsInRoleAsync(user.Id,"Doctor");
                if(valid)
                {
                    doctors.Add(user);
                }
            }
            ConsultationViewModel vm = new ConsultationViewModel();
            vm.PacientPNC = pnc;
            vm.Doctors = doctors;
            return View(vm);
        }

        // POST: Pacient/Delete/5
        [System.Web.Mvc.Authorize(Roles = "Admin, Secretary")]
        [HttpPost]
        public ActionResult Schedule(ConsultationViewModel consultation)
        {
            try
            {
                var t=Mapper.Map<ConsultationDTO>(consultation);
                consultation.AvailableHours=_ConsultationUnitOfWork.GetConsultationWithAvailableHours(t);
                consultation.Doctor = _UserUnitOfWork.GetById(consultation.DoctorId);
                return View("Appoint", consultation);
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        [System.Web.Mvc.Authorize(Roles = "Admin, Secretary")]
        public ActionResult Appoint(ConsultationViewModel consultation)
        {
            try
            {
                var t = Mapper.Map<ConsultationDTO>(consultation);

                _ConsultationUnitOfWork.Create(t);
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }
        [System.Web.Mvc.Authorize(Roles = "Admin, Secretary, Doctor")]
        public ActionResult Consultations(string pnc)
        {
            var consults=_ConsultationUnitOfWork.GetAll();
            var consultVM=Mapper.Map<IEnumerable<ConsultationDTO>, IEnumerable<ConsultationViewModel>>(consults);
            foreach (var consultation in consultVM)
            {
                consultation.Doctor = _UserUnitOfWork.GetById(consultation.DoctorId);
            }
            return View(consultVM);
        }

        [System.Web.Mvc.Authorize(Roles = "Admin, Doctor")]
        public ActionResult EditConsultation(int  id)
        {
            var t = Mapper.Map<ConsultationViewModel>(_ConsultationUnitOfWork.GetById(id));
            return View(t);
        }

        // POST: Pacient/Edit/5
        [System.Web.Mvc.Authorize(Roles = "Admin, Doctor")]
        [HttpPost]
        public ActionResult EditConsultation(ConsultationViewModel consultation)
        {
            try
            {
                var t = Mapper.Map<ConsultationDTO>(consultation);
                _ConsultationUnitOfWork.Update(t);

                return RedirectToAction("Consultations", "Pacient", new { pnc = consultation.PacientPNC });
            }
            catch
            {
                return View();
            }
        }
        [System.Web.Mvc.Authorize(Roles = "Admin, Secretary")]
        public ActionResult CheckIn(int id)
        {
            var t=_ConsultationUnitOfWork.GetById(id);
            var pacient=_PacientUnitOfWork.GetById(t.PacientPNC);
            Console.WriteLine(t.DoctorId);
            var doctor=_UserUnitOfWork.GetById(t.DoctorId);
            
            
            var context = GlobalHost.ConnectionManager.GetHubContext<ClinicHub>();
            context.Clients.User(doctor.UserName).addNewMessageToPage("Pacient " + pacient.Name + " has checked in for the " + t.Hour + " o'clock consultation");
            
            
            t.Description = "Checked in";
            _ConsultationUnitOfWork.Update(t);
            return RedirectToAction("Index");
        }
    }
}