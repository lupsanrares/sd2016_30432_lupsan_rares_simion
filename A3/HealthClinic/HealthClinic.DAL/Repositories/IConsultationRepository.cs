﻿using HealthClinic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthClinic.DAL.Repositories
{
    public interface IConsultationRepository : IEntityRepository<Consultation, ConsultationDTO>
    {
        IEnumerable<ConsultationDTO> GetByDateAndDoctor(DateTime date, String id);
        ConsultationDTO GetById(int Id);
    }
}
