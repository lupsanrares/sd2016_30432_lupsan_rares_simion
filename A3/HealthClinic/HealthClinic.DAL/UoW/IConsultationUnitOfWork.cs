﻿using HealthClinic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthClinic.DAL.UoW
{
    public interface IConsultationUnitOfWork:IUnitOfWork<ConsultationDTO>
    {
        ConsultationDTO GetById(int Id);
        List<HealthClinic.Model.Enums.Hours> GetConsultationWithAvailableHours(ConsultationDTO consultation);
    }
}
