﻿using Bank.Model;
using Bank.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.DataAccess;
using System.Threading;

namespace Bank.BusinessLogic
{
    public class UserService : Service<UserDTO>, IUserService
    {
        IUserUnitOfWork _user;
        public UserService(IUserUnitOfWork user)
            : base(user)
        {
            _user = user;
        }

        public override IEnumerable<UserDTO> GetAll()
        {
            return _user.GetAll().ToList();
        }

        public  IEnumerable<ReportDTO> GetByDate(String Id, DateTime first, DateTime second)
        {
            return _user.GetByDate(Id,first,second);
        }

        public UserDTO GetById(string pnc)
        {
            return _user.GetById(pnc);
        }

        public override void Create(UserDTO client)
        {
            if (client == null)
            {
                return;
            }
            if (GetById(client.Id) == null)
            {
                _user.Create(client);
                _user.SaveChanges();
               
            }
        }

        public override void Delete(UserDTO entity)
        {
            if (entity == null)
            {
                return;
            }
            if (GetById(entity.Id)!= null)
            {
                _user.Delete(entity);
                _user.SaveChanges();
            }
        }

        public override void Update(UserDTO entity)
        {
            if (entity == null)
            {
                return;
            }
            if (GetById(entity.Id) != null)
            {
                _user.Update(entity);
                _user.SaveChanges();
            }
        }
    }
}
