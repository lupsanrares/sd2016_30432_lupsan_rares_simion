﻿using Bank.Model;
using Bank.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.BusinessLogic
{
    public interface IClientService : IService<ClientDTO>
    {
        ClientDTO GetById(string Id);
       
    }
}
