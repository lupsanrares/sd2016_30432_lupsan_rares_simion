﻿using Bank.DataAccess;
using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bank.BusinessLogic
{
    public class ClientUnitOfWork:IClientUnitOfWork
    {
        #region Fields
        private readonly bankEntities _context;
        private IClientRepository _clientRepository;
        private IBankAccountRepository _accountRepository;
        private IReportRepository _reportRepository;
       
        #endregion

        #region Constructors
        public ClientUnitOfWork(IClientRepository repository,IBankAccountRepository accountRepository,IReportRepository reportRepository)
        {
            _context = new bankEntities();
            _clientRepository= repository;
            _accountRepository = accountRepository;
            _reportRepository = reportRepository;
        }
        #endregion

        #region IUnitOfWork Members
        public IClientRepository ClientRepository
        {
            get { return _clientRepository ?? (_clientRepository = new ClientRepository(_context)); }
        }

        public IEnumerable<ClientDTO> GetAll()
        {
            return _clientRepository.GetAll();
        }


        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            _clientRepository = null;
            _accountRepository = null;
            _reportRepository = null;
            _context.Dispose();
        }
        #endregion


        public void Create(ClientDTO entity)
        {
            _clientRepository.Create(entity);
        }

        public void Delete(ClientDTO entity)
        {
           
            var accounts=_accountRepository.GetAll().ToList();
            accounts=accounts.Where(t=>t.ClientPNC.Equals(entity.PNC)).ToList();
            foreach(var account in accounts)
            {
                _accountRepository.Delete(account);
            }
            _clientRepository.Delete(entity);
        }

        public void Update(ClientDTO entity)
        {   
            _clientRepository.Update(entity);
        }

        public ClientDTO GetById(string pnc)
        {
            return _clientRepository.GetById(pnc);
        }
        public void Report(ReportDTO report)
        {
            _reportRepository.Create(report);
        }
        
    }
    
}
