﻿using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.DataAccess
{
    public interface IReportRepository : IEntityRepository<Report, ReportDTO>
    {
        IEnumerable<ReportDTO> GetByDate(String Id,DateTime first, DateTime second);
    }
}
