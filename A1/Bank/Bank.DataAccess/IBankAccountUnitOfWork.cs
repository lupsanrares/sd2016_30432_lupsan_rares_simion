﻿using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.DataAccess
{
    public interface IBankAccountUnitOfWork : IUnitOfWork<BankAccountDTO>
    {
        BankAccountDTO GetById(String iban);
    }
}
