﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.DataAccess
{

    public interface IContext
    {
         IDbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
         IDbSet<AspNetRole> AspNetRoles { get; set; }
         IDbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
         IDbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
         IDbSet<AspNetUser> AspNetUsers { get; set; }
         IDbSet<BankAccount> BankAccounts { get; set; }
         IDbSet<Client> Clients { get; set; }
         IDbSet<Report> Reports { get; set; }

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        int SaveChanges();
    }

}
