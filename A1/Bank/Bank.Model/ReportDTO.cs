﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class ReportDTO
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Description { get; set; }
        public System.DateTime Creation { get; set; }
    }
}
