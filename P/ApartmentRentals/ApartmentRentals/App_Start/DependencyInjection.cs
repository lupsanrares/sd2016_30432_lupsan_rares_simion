﻿using ApartmentRentals.DataAccess;
using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ApartmentRentals.App_Start
{
    public class DependencyInjection:Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType(typeof(apartmentrentalsEntities)).As(typeof(IContext)).InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.Load("ApartmentRentals.BL"))

                      .Where(t => t.Name.EndsWith("Service"))

                      .AsImplementedInterfaces()

                      .InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.Load("ApartmentRentals.DataAccess"))

                    .Where(t => t.Name.EndsWith("UnitOfWork"))

                    .AsImplementedInterfaces()

                    .InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.Load("ApartmentRentals.DataAccess"))

                   .Where(t => t.Name.EndsWith("Repository"))

                   .AsImplementedInterfaces()

                   .InstancePerLifetimeScope();
        }
    }
}