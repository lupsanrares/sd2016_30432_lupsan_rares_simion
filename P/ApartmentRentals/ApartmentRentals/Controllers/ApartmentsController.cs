﻿using ApartmentRentals.BL;
using ApartmentRentals.Model;
using ApartmentRentals.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ApartmentRentals.Hubs;
using Microsoft.AspNet.SignalR;

namespace ApartmentRentals.Controllers
{
    public class ApartmentsController : Controller
    {
        IApartmentService _apartmentService;
        public ApartmentsController(IApartmentService apartmentService)
        {
            _apartmentService = apartmentService;
        }
        // GET: Apartments
        public ActionResult Index()
        {
            var list = Mapper.Map<IEnumerable<ApartmentViewModel>>(_apartmentService.GetAll());
            return View(list);
        }

        // GET: Apartments/Details/5
        public ActionResult Details(int id)
        {
            var apartment = Mapper.Map<ApartmentViewModel>(_apartmentService.GetById(id));
            return View(apartment);
        }

        // GET: Apartments/Create
        [System.Web.Mvc.Authorize(Roles = "Owner")]
        public ActionResult Create()
        {
            var aptVm = new ApartmentViewModel();
            aptVm.Amenities = _apartmentService.GetAllAmenities();
            return View(aptVm);
        }

        // POST: Apartments/Create
        [System.Web.Mvc.Authorize(Roles = "Owner")]
        [HttpPost]
        public ActionResult Create(ApartmentViewModel aptViewModel)
        {
            try
            {
                var apt = Mapper.Map<ApartmentDTO>(aptViewModel);
                apt.ImagesUrl = new List<string>();
                // TODO: Add insert logic here
                foreach (var file in aptViewModel.Files)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        var t= Guid.NewGuid() + Path.GetExtension(file.FileName);
                        string fileName = Path.Combine(Server.MapPath("/uploads"),t);
                        file.SaveAs(fileName);
                        var x = Path.Combine("/uploads", t);
                        apt.ImagesUrl.Add(x);

                    }

                }
                apt.Owner_Id = User.Identity.GetUserId();
                apt.Status_Id = 1;
                _apartmentService.Create(apt);
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        // GET: Apartments/Edit/5
        [System.Web.Mvc.Authorize(Roles = "Owner")]
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Apartments/Edit/5
        [System.Web.Mvc.Authorize(Roles = "Owner")]
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Apartments/Delete/5
        [System.Web.Mvc.Authorize(Roles = "Owner")]
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Apartments/Delete/5
        [System.Web.Mvc.Authorize(Roles = "Owner")]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [System.Web.Mvc.Authorize(Roles = "Tenant")]
        [HttpPost]
        public ActionResult Offer(OfferDTO offer)
        {
            try
            {
                offer.Date = DateTime.Now;
                offer.Status_Id = "Sent";
                offer.Response = "";
                _apartmentService.MakeOffer(offer);
                return RedirectToAction("Index", "Apartments");
            }
            catch
            {
                return View();
            }
        }
        [System.Web.Mvc.Authorize(Roles = "Owner")]
        [HttpPost]
        public ActionResult Accept(OfferDTO offer)
        {
            try
            {
                _apartmentService.AcceptOffer(offer);
                var context = GlobalHost.ConnectionManager.GetHubContext<ApartmentHub>();
                context.Clients.All.addNewMessageToPage("Appartment " + offer.Apartment_Id + " has been booked");

                return RedirectToAction("Index", "Apartments");
            }
            catch
            {
                return View();
            }
        }
        [System.Web.Mvc.Authorize(Roles = "Owner")]
            [HttpPost]
        public ActionResult Refuse(OfferDTO offer)
        {
            try
            {
                _apartmentService.RefuseOffer(offer);
                return RedirectToAction("Index", "Apartments");
            }
            catch
            {
                return View();
            }
        }
    }
}
