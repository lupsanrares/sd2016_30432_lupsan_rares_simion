﻿using ApartmentRentals.DataAccess.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.BL
{
    public abstract class Service<T> : IService<T> where T : class
    {
        protected IUnitOfWork<T> _unitOfWork;

        public Service(IUnitOfWork<T> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public virtual void Create(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _unitOfWork.Create(entity);
            _unitOfWork.SaveChanges();
        }


        public virtual void Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.Update(entity);
            _unitOfWork.SaveChanges();
        }

        public virtual void Delete(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.Delete(entity);
            _unitOfWork.SaveChanges();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _unitOfWork.GetAll();
        }
    }
}