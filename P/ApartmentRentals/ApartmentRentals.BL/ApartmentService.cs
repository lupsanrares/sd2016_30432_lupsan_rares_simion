﻿using ApartmentRentals.DataAccess.UnitsOfWork;
using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.BL
{
    public class ApartmentService:Service<ApartmentDTO>,IApartmentService
    {
        IApartmentUnitOfWork _apartmentUnitOfWork;
        public ApartmentService(IApartmentUnitOfWork aptUot)
            : base(aptUot)
        {
            _apartmentUnitOfWork = aptUot;

        }
        public IEnumerable<AmenitiesDTO> GetAllAmenities()
        {
            return _apartmentUnitOfWork.GetAllAmenities();
        }

        public ApartmentDTO GetById(int id)
        {
            return _apartmentUnitOfWork.GetById(id);
        }


        public void MakeOffer(OfferDTO offer)
        {
            _apartmentUnitOfWork.MakeOffer(offer);
        }


        public void AcceptOffer(OfferDTO offer)
        {
            _apartmentUnitOfWork.AcceptOffer(offer);
         
        }

        public void RefuseOffer(OfferDTO offer)
        {
            _apartmentUnitOfWork.RefuseOffer(offer);
        }
    }
}
