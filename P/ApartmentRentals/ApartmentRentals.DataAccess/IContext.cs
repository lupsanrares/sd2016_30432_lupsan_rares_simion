﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess
{
    public interface IContext
    {
        IDbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        IDbSet<Apartment> Apartments { get; set; }
        IDbSet<Apartment_Status> Apartment_Status { get; set; }
        IDbSet<Apartment_Type> Apartment_Type { get; set; }
        IDbSet<AspNetRole> AspNetRoles { get; set; }
        IDbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        IDbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        IDbSet<AspNetUser> AspNetUsers { get; set; }
        IDbSet<Contract> Contracts { get; set; }
        IDbSet<Facility> Facilities { get; set; }
        IDbSet<Image> Images { get; set; }
        IDbSet<Message> Messages { get; set; }
        IDbSet<Offer> Offers { get; set; }

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        int SaveChanges();
    }
}
