﻿using ApartmentRentals.Model;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public class MessageRepository:EntityRepository<Message,MessageDTO>,IMessageRepository
    {
        IContext _context;
        public MessageRepository(IContext context)
             : base(context)
         {
             _context = context;
             _dbset = _context.Set<Message>();
         }
        public MessageDTO GetById(string id)
        {
            var apt = _dbset.Where(t => t.Id.Equals(id)).FirstOrDefault();
            return Mapper.Map<MessageDTO>(apt);
        }
        public IEnumerable<string> GetAllConversations(string userId)
        {
            var list = _dbset.Where(t => t.Sender_Id.Equals(userId)).Select(t=>t.Receiver_Id).ToList();
            list.AddRange(_dbset.Where(t => t.Receiver_Id.Equals(userId)).Select(t => t.Sender_Id).ToList());
            return list.Distinct();
        }
        public IEnumerable<MessageDTO> GetConversation(string Id1,string Id2)
        {
            var list = _dbset.Where(t => t.Sender_Id.Equals(Id1)&&t.Receiver_Id.Equals(Id2)).ToList();
            list.AddRange(_dbset.Where(t => t.Sender_Id.Equals(Id2)&&t.Receiver_Id.Equals(Id1)).ToList());
            return Mapper.Map<IEnumerable<MessageDTO>>(list);
            
        }
    }
}
