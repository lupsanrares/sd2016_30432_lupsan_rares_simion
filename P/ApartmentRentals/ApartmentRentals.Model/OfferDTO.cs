﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.Model
{
    public class OfferDTO
    {
        public int Id { get; set; }
        public string Landlord_Id { get; set; }
        public string Tenant_Id { get; set; }
        public System.DateTime Date { get; set; }
        public int Apartment_Id { get; set; }
        public double Fee { get; set; }
        public string Status_Id { get; set; }
        public string Response { get; set; }

        public virtual UserDTO AspNetUser { get; set; }
        public virtual UserDTO AspNetUser1 { get; set; }
        public ApartmentDTO Apartment { get; set; }
    }
}
