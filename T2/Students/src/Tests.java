import static org.junit.Assert.*;

import java.sql.Date;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Course;
import model.Enrollment;
import model.Student;
import dal.CourseDAO;
import dal.DAOFactory;
import dal.EnrollmentDAO;
import dal.StudentDAO;


public class Tests {
	private static Student firstStudent=null;
	private static Student secondStudent=null;
	private static Student thirdStudent=null;
	private static Course firstCourse=null;
	private static Course secondCourse=null;
	private static Course thirdCourse=null;
	private static Enrollment firstEnrol=null;
	private static Enrollment secondEnrol=null;
	
	@BeforeClass
	public static void testCreateStudent() {
		StudentDAO stud=DAOFactory.getStudentDAO();
		CourseDAO course=DAOFactory.getCourseDAO();
		EnrollmentDAO enrol=DAOFactory.getEnrollmentDAO();
		java.util.Date b=new java.util.Date("1994/02/05");
		Date birth=new Date(b.getTime());
		secondStudent=new Student("Lupsan Adrian","10 Apicultorilor,Bistrita, Bistrita-Nasaud",birth);
		int second=stud.create(secondStudent);
		secondStudent.setID(second);
		
		b=new java.util.Date("1994/08/20");
		birth=new Date(b.getTime());
		
		thirdStudent=new Student("Toniuc Daniel","2 Piata Centrala, Bistrita, Bistrita-Nasaud",birth); 
		int third=stud.create(thirdStudent);
		
		thirdStudent.setID(third);
		
		secondCourse=new Course("Introduction to Artificial Intelligence", "Letia Alfred", 3);
		int course1=course.create(secondCourse);
		secondCourse.setcID(course1);
		thirdCourse=new Course("Functional Programming", "Groza Adrian", 3);
		int course2=course.create(thirdCourse);
		thirdCourse.setcID(course2);
		secondEnrol=new Enrollment(second,course1);
		enrol.create(secondEnrol);
		
	}
	
	@Test
	public void testInsertStudent(){
		StudentDAO stud=DAOFactory.getStudentDAO();
		int count=stud.readAll().size();
		java.util.Date b=new java.util.Date("1995/06/08");
		Date birth=new Date(b.getTime());
		firstStudent=new Student("Lupsan Rares","96 Principala, Lesu, Bistrita-Nasaud",birth);
		int first=stud.create(firstStudent);
		firstStudent.setID(first);
		int count2=stud.readAll().size();
		assertTrue(count==(count2-1));
	}
	@Test
	public void testReadStudent(){
		StudentDAO stud=DAOFactory.getStudentDAO();
		java.util.Date b=new java.util.Date("1994/04/01");
		Date birth=new Date(b.getTime());
		 
		int third=stud.create(new Student("Pop Daniel","7 Simpozionului, Bistrita, Bistrita-Nasaud",birth));
		Student ton=stud.read(third);
		assertTrue(ton.getName().equals("Pop Daniel"));
		assertTrue(ton.getAddress().equals("7 Simpozionului, Bistrita, Bistrita-Nasaud"));
		
	}
	@Test
	public void testUpdateStudent(){
		StudentDAO stud=DAOFactory.getStudentDAO();
		secondStudent.setName("Lupsan Cristian");
		stud.update(secondStudent);
		Student temp=stud.read(secondStudent.getID());
		assertTrue(temp.getName().equals("Lupsan Cristian"));
	}
	@Test
	public void testDeleteStudent(){
		StudentDAO stud=DAOFactory.getStudentDAO();
		int studentsi=stud.readAll().size();
		stud.delete(thirdStudent.getID());
		int students=stud.readAll().size();
		assertTrue(studentsi==(students+1));
	}
	
	@Test
	public void testInsertCourse(){
		CourseDAO c=DAOFactory.getCourseDAO();
		int count=c.readAll().size();
		firstCourse=new Course("Digital Microprocessing Systems", "Negru Mihai", 3);
		int course1=c.create(firstCourse);
		firstCourse.setcID(course1);
		int count2=c.readAll().size();
		assertTrue(count==(count2-1));
	}
	@Test
	public void testReadCourse(){
		CourseDAO c=DAOFactory.getCourseDAO();
		int course1=c.create(new Course("Digital system design", "Cret Octavian", 2));
		
		Course first =c.read(course1);
		assertTrue(first.getcName().equals("Digital system design"));
		assertTrue(first.getTeacher().equals("Cret Octavian"));
		assertTrue(first.getStudyYear()==2);
	}
	@Test
	public void testUpdateCourse(){
		CourseDAO c=DAOFactory.getCourseDAO();
		secondCourse.setcName("Software Engineering");
		c.update(secondCourse);
		Course temp=c.read(secondCourse.getcID());
		assertTrue(temp.getcName().equals("Software Engineering"));
	}
	@Test
	public void testDeleteCourse(){
		CourseDAO c=DAOFactory.getCourseDAO();
		int coursesi=c.readAll().size();
		c.delete(thirdCourse.getcID());
		int courses=c.readAll().size();
		assertTrue(coursesi==(courses+1));
	}
	@Test
	public void testCreateEnrollment(){
		
		EnrollmentDAO e=DAOFactory.getEnrollmentDAO();
		int enrolls=e.readAll().size();
		firstEnrol=new Enrollment(firstStudent.getID(),secondCourse.getcID());
		e.create(firstEnrol);
		int enroll2=e.readAll().size();
		assertTrue(enroll2==(enrolls+1));
	}
	@Test
	public void testReadStudentsFromCourse(){
		EnrollmentDAO e=DAOFactory.getEnrollmentDAO();
		ArrayList<Student> students=e.read(secondCourse.getcName());
		System.out.println("Students enrolled in course: "+secondCourse.getcName());
		for(int i=0;i<students.size();i++)
		{
			System.out.println(students.get(i).getName());
		}
	}
	
	@AfterClass
	public static void testClearTables()
	{
		EnrollmentDAO enrol=DAOFactory.getEnrollmentDAO();
		//enrol.delete(firstEnrol.getcID(),firstEnrol.getsID());
		enrol.delete(secondEnrol.getcID(),secondEnrol.getsID());
		StudentDAO stud=DAOFactory.getStudentDAO();
		stud.delete(firstStudent.getID());
		stud.delete(secondStudent.getID());
		CourseDAO course=DAOFactory.getCourseDAO();
		course.delete(firstCourse.getcID());
		course.delete(secondCourse.getcID());

	}
}
