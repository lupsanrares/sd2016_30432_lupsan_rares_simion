package model;

public class Course {
private int cID;
private String cName;
private String teacher;
private int studyYear;

public Course(String cName,String teacher,int studyYear)
{
	this.cName=cName;
	this.teacher=teacher;
	this.studyYear=studyYear;
}

public Course(int cId,String cName,String teacher,int studyYear)
{
	this.cID=cId;
	this.cName=cName;
	this.teacher=teacher;
	this.studyYear=studyYear;
}

public int getcID() {
	return cID;
}
public void setcID(int cID) {
	this.cID = cID;
}
public String getcName() {
	return cName;
}
public void setcName(String cName) {
	this.cName = cName;
}
public String getTeacher() {
	return teacher;
}
public void setTeacher(String teacher) {
	this.teacher = teacher;
}
public int getStudyYear() {
	return studyYear;
}
public void setStudyYear(int studyYear) {
	this.studyYear = studyYear;
}

}
