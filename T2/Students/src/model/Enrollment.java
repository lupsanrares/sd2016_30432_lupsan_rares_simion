package model;

public class Enrollment {

private int eID;
private int sID;
private int cID;

public Enrollment(int sID,int cID)
{
	this.sID=sID;
	this.cID=cID;
}
public int geteID() {
	return eID;
}
public void seteID(int eID) {
	this.eID = eID;
}
public int getsID() {
	return sID;
}
public void setsID(int sID) {
	this.sID = sID;
}
public int getcID() {
	return cID;
}
public void setcID(int cID) {
	this.cID = cID;
}
}
