package model;
import java.sql.Date;


public class Student {
private int ID;
private String name;
private String address;
private Date birthDate;


public Student(String name,String address, Date birthDate) 
{
	if(name.equals(""))
	{
		throw new IllegalArgumentException("Student Name can not be null");
	}
	this.name=name;
	this.address=address;
	this.birthDate=birthDate;
}
public Student(int iD, String name,String address, Date birthDate) 
{
	if(iD<0)
	{
		throw new IllegalArgumentException("Student ID must pe a positive integer");
	}
	if(name.equals(""))
	{
		throw new IllegalArgumentException("Student Name can not be null");
	}
	this.ID=iD;
	this.name=name;
	this.address=address;
	this.birthDate=birthDate;
}

public int getID() {
	return ID;
}
public void setID(int iD) {
	ID = iD;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public Date getBirthDate() {
	return birthDate;
}
public void setBirthDate(Date birthDate) {
	this.birthDate = birthDate;
}
}
