package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Course;

public class CourseDAO {

	
	private static final String CREATE_QUERY = "INSERT INTO course (CName,Teacher,StudyYear) VALUES (?,?,?)";
    /** The query for read. */
    private static final String READ_QUERY = "SELECT  * FROM course WHERE CID = ?";
    /** The query for reading all courses. */
    private static final String READ_ALL_QUERY = "SELECT * FROM course";
    /** The query for update. */
    private static final String UPDATE_QUERY = "UPDATE course SET CName=? , Teacher=?, StudyYear=? WHERE CID = ?";
    /** The query for delete. */
    private static final String DELETE_QUERY = "DELETE FROM course WHERE CID = ?";
    /** The query for deleting all courses. */
    private static final String DELETE_ALL_QUERY = "Delete from course";
 
    public int create(Course course) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(CREATE_QUERY,Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, course.getcName());
            preparedStatement.setString(2, course.getTeacher());
            preparedStatement.setInt(3, course.getStudyYear());
            preparedStatement.execute();
            result = preparedStatement.getGeneratedKeys();
 
            if (result.next() && result != null) {
                return result.getInt(1);
            } else {
                return -1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
 
        return 0;
    }
 
    public Course read(int ID) {
        Course course = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(READ_QUERY);
            preparedStatement.setInt(1, ID);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
 
            if (result.next() && result != null) {
                course =new Course(result.getInt(1),result.getString(2),result.getString(3),result.getInt(4));
            } else {
                // TODO
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
 
        return course;
    }
    public ArrayList<Course> readAll() {
        ArrayList <Course> course = new ArrayList<Course>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(READ_ALL_QUERY);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
 
            while (result.next() && result != null) {
                Course stud = new Course(result.getInt(1),result.getString(2),result.getString(3),result.getInt(4));
                course.add(stud);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
 
        return course;
    }
    public boolean update(Course course) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, course.getcName());
            preparedStatement.setString(2, course.getTeacher());
            preparedStatement.setInt(3, course.getStudyYear());
            preparedStatement.setInt(4, course.getcID());
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
               sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
        return false;
    }
 
    public boolean delete(int ID) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(DELETE_QUERY);
            preparedStatement.setInt(1, ID);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
           e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
        return false;
    }
    public Boolean deleteAll() {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(DELETE_ALL_QUERY);
            preparedStatement.execute();
            return true;
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
            	conn.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
 
        return false;
    }
 
}