package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Course;
import model.Enrollment;
import model.Student;

public class EnrollmentDAO {

	private static final String CREATE_QUERY = "INSERT INTO enrollment (Stud_ID,C_ID) VALUES (?,?)";
    /** The query for read. */
    private static final String READ_STUDENTS_OF_COURSE_QUERY = "SELECT s.* FROM  enrollment AS e INNER JOIN student AS s ON (e.Stud_ID=s.ID) INNER JOIN course AS c ON (e.C_ID=c.CID)  WHERE c.CName = ?";
    /** The query for reading all enrollments. */
    private static final String READ_ALL_QUERY = "SELECT * FROM enrollment";
     /** The query for delete. */
    private static final String DELETE_QUERY = "DELETE FROM enrollment WHERE C_ID = ? AND Stud_ID=?";
    /** The query for deleting all enrollments. */
    private static final String DELETE_ALL_QUERY = "DELETE  FROM enrollment";
 
    public int create(Enrollment enrol) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(CREATE_QUERY,Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, enrol.getsID());
            preparedStatement.setInt(2, enrol.getcID());
            preparedStatement.execute();
            result = preparedStatement.getGeneratedKeys();
 
            if (result.next() && result != null) {
                return result.getInt(1);
            } else {
                return -1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
 
        return 0;
    }
 
    public ArrayList<Student> read(String cName) {
        ArrayList<Student> students = new ArrayList<Student>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(READ_STUDENTS_OF_COURSE_QUERY);
            preparedStatement.setString(1, cName);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
 
           while(result.next() && result != null) {
                students.add(new Student(result.getInt(1),result.getString(2),result.getString(3),result.getDate(4)));
            } 
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
 
        return students;
    }
    public ArrayList<Enrollment> readAll() {
        ArrayList <Enrollment> enrols = new ArrayList<Enrollment>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(READ_ALL_QUERY);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
 
            while (result.next() && result != null) {
            	Enrollment en = new Enrollment(result.getInt(1),result.getInt(2));
                enrols.add(en);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
   
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
 
        return enrols;
    }
    public boolean delete(int CID, int SID) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(DELETE_QUERY);
            preparedStatement.setInt(1, CID);
            preparedStatement.setInt(2, SID);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
           e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
        return false;
    }
    public Boolean deleteAll() {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(DELETE_ALL_QUERY);
            preparedStatement.execute();
            return true;
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
            	conn.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
 
        return false;
    }
}
