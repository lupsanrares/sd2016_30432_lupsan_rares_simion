package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;




import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Student;

public class StudentDAO {
	
	private static final String CREATE_QUERY = "INSERT INTO student (Name,Address,BirthDate) VALUES (?,?,?)";
    /** The query for read. */
    private static final String READ_QUERY = "SELECT * FROM student WHERE ID = ?";
    /** The query for reading all students. */
    private static final String READ_ALL_QUERY = "SELECT * FROM student";
    /** The query for update. */
    private static final String UPDATE_QUERY = "UPDATE student SET Name=? , Address=?, BirthDate=? WHERE ID = ?";
    /** The query for delete. */
    private static final String DELETE_QUERY = "DELETE FROM student WHERE ID = ?";
    /** The query for deleting all enrollments. */
    private static final String DELETE_ALL_QUERY = "DELETE FROM student";
 
    public int create(Student student) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(CREATE_QUERY,Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, student.getName());
            preparedStatement.setString(2, student.getAddress());
            preparedStatement.setDate(3, student.getBirthDate());
            preparedStatement.execute();
            result = preparedStatement.getGeneratedKeys();
 
            if (result.next() && result != null) {
                return result.getInt(1);
            } else {
                return -1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
 
        return 0;
    }
 
    public Student read(int ID) {
        Student student = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(READ_QUERY);
            preparedStatement.setInt(1, ID);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
 
            if (result.next() && result != null) {
                student =new Student(result.getInt(1),result.getString(2),result.getString(3),result.getDate(4));
            } else {
                // TODO
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
 
        return student;
    }
    public ArrayList<Student> readAll() {
        ArrayList <Student> student = new ArrayList<Student>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(READ_ALL_QUERY);
            preparedStatement.execute();
            result = preparedStatement.getResultSet();
 
            while (result.next() && result != null) {
                Student stud = new Student(result.getInt(1),result.getString(2),result.getString(3),result.getDate(4));
                student.add(stud);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
 
        return student;
    }
    public boolean update(Student student) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, student.getName());
            preparedStatement.setString(2, student.getAddress());
            preparedStatement.setDate(3, student.getBirthDate());
            preparedStatement.setInt(4, student.getID());
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
               sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
                cse.printStackTrace();
            }
        }
        return false;
    }
 
    public boolean delete(int ID) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(DELETE_QUERY);
            preparedStatement.setInt(1, ID);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
           e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
        return false;
    }
    public Boolean deleteAll() {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        try {
            conn = DAOFactory.createConnection();
            preparedStatement = conn.prepareStatement(DELETE_ALL_QUERY);
            preparedStatement.execute();
            return true;
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception rse) {
                rse.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (Exception sse) {
                sse.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception cse) {
               cse.printStackTrace();
            }
        }
 
        return false;
    }
}