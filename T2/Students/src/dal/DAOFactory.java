package dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DAOFactory {
	 /** The driver-class. */
    public static final String DRIVER = "com.mysql.jdbc.Driver";
    /** The url to database. */
    public static final String DBURL = "jdbc:mysql://localhost:3306/university?autoReconnect=true&useSSL=false";
    /** The username for database-operations. */
    public static final String USER = "root";
    /** The password for database-operations. */
    public static final String PASS = "admin";
    /**
     * Method to create a Connection on the mysql-database.
     * 
     * @return the Connection.
     */
    public static Connection createConnection() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DBURL, USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } 
        return conn;
    }
    public static StudentDAO getStudentDAO()
    {
    	return new StudentDAO();
    }
    public static CourseDAO getCourseDAO()
    {
    	return new CourseDAO();
    }
    public static EnrollmentDAO getEnrollmentDAO()
    {
    	return new EnrollmentDAO();
    }
}
