import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.swing.JFrame;


public class University {
	private Connection connect;
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					University window = new University();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public University() {
		initialize();
		populate();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		connect=this.getConnection();
	}
	private void populate()
	{
		
	}
	private Connection getConnection()
	{
		Connection conn=null;
		Properties connectionProp=new Properties();
		connectionProp.put("user", "root");
		connectionProp.put("password", "");
		try {
			conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/",connectionProp);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Connected to database succesfully");
		return conn;
	}
}
