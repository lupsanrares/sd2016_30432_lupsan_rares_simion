﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore.DataAccess
{
    public interface IAuthorRepository
    {
        Author GetById(int id);
        Author GetByName(String name);
        IEnumerable<Author> SearchByName(String name);
        int Insert(String authorName);
    }
}
