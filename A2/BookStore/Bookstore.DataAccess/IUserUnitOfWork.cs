﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore.DataAccess
{
    public interface IUserUnitOfWork
    {
        User Login(String username, String password);
        IEnumerable<User> GetAll();
        List<string> GetRoles(User user);
        void Create(User user);
        User FindById(string userId);
        void Delete(String id);
        void Update(User user);

    }
}
