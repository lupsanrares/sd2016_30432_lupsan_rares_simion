﻿using BookStore.Models;
using FileHelpers;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Bookstore.DataAccess
{
    public abstract class Report
    {
         
        private String _filename = null;
        public Report(List<Book> books)
        {
            _filename=this.GenerateReport(books);
        }
        public String FileName
        {
            get { return _filename; }
        }
        public abstract string GenerateReport(List<Book> Books);
    }
    public class PDFReport:Report
    {
        public PDFReport(List<Book> books):base(books)
        {
        }
        public override string GenerateReport(List<Book> Books)
        {
            var doc1 = new Document();
            string path = HttpContext.Current.Server.MapPath("../OutOfStockBooks.pdf");

            PdfWriter.GetInstance(doc1, new FileStream(path, FileMode.Create));
            doc1.Open();
            PdfPTable table = new PdfPTable(6);
            table.AddCell("Id");
            table.AddCell("Title");
            table.AddCell("AuthorId");
            table.AddCell("GenreId");
            table.AddCell("Quantity");
            table.AddCell("Price");
            foreach(Book book in Books)
            {
                table.AddCell(book.Id.ToString());
                table.AddCell(book.Title.ToString());
                table.AddCell(book.AuthorId.ToString());
                table.AddCell(book.GenreId.ToString());
                table.AddCell(book.Quantity.ToString());
                table.AddCell(book.Price.ToString());
            }

            doc1.Add(table);
            doc1.Close();
            return path;
        }
    }
    public class CSVReport : Report
    {
        public CSVReport(List<Book> books):base(books)
        {
        }
        public override string GenerateReport(List<Book> dataSource)
        {
             try
            {
                //filehelper object
                FileHelperEngine engine = new FileHelperEngine(typeof(Book));
                //give file a name and header text
                string filename = "../OutOfStockBooks.csv";
                engine.HeaderText = "Id,Title,AuthorId,GenreId,Quantity,Price";
                //save file locally
                engine.WriteFile(HttpContext.Current.Server.MapPath(filename), dataSource);
                return HttpContext.Current.Server.MapPath(filename);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
