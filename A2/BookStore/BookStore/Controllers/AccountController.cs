﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using BookStore.Models;
using BookStore.App_Start;
using Bookstore.DataAccess;

namespace BookStore.Controllers
{
    public class AccountController : Controller
    {
        IUserUnitOfWork _UserUnitOfWork;
        public AccountController(IUserUnitOfWork userUoW)
        {
            _UserUnitOfWork = userUoW;
        }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User model, string returnUrl)
        {
            var user=_UserUnitOfWork.Login(model.UserName, model.PasswordHash);
            if(user==null)
            {
                ModelState.AddModelError("", "Invalid login attempt.");
                return View(model);
            }
            Session["user"] = new User() {Id=user.Id, Name = user.Name, Roles=user.Roles };
            return RedirectToAction("Index","Home");
        }

        ////
        //// GET: /Account/Register
        //public ActionResult Register()
        //{
        //    return View();
        //}

        ////
        //// POST: /Account/Register
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Register(RegisterViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = new User() { Id = model.Email, UserName = model.Email, Name = model.Name, Address = model.Address, PhoneNumber = model.PhoneNumber };
                
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}
 
        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            Session["user"] = null;
            return RedirectToAction("Index", "Home");
        }



    }
}