﻿using Bookstore.DataAccess;
using BookStore.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using BookStore.App_Start;

namespace BookStore.App_Start
{
    public class CustomAuthorizationAttribute : AuthorizeAttribute
    {
        public string IdentityRoles
        {
            get { return _identityRoles ?? String.Empty; }
            set
            {
                _identityRoles = value;
                _identityRolesSplit = SplitString(value);
            }
        }

        private string _identityRoles;
        private string[] _identityRolesSplit = new string[0];

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (_identityRolesSplit.Length > 0)
            {
                //get the UserManager
                var user = HttpContext.Current.Session["user"] as User;
                if (user == null)
                    return false;
                var roles = user.Roles;
                //if the at least one of the Roles of the User is in the IdentityRoles list return true
                if (_identityRolesSplit.Any(roles.Contains))
                {
                    return true;
                }

                return false;
            }
            else
            {
                return true;
            }

        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //if the user is not logged in use the deafult HandleUnauthorizedRequest and redirect to the login page
            if (filterContext.HttpContext.Session["user"]==null)
            {
                filterContext.Result = new RedirectResult("/Account/Login");
            }
            else
            //if the user is logged in but is trying to access a page he/she doesn't have the right for show the access denied page
            {
                filterContext.Result = new RedirectResult("~/Home/AccessDenied");
            }
        }

        protected static string[] SplitString(string original)
        {
            if (String.IsNullOrEmpty(original))
            {
                return new string[0];
            }

            var split = from piece in original.Split(',')
                        let trimmed = piece.Trim()
                        where !String.IsNullOrEmpty(trimmed)
                        select trimmed;
            return split.ToArray();
        }
    }
}