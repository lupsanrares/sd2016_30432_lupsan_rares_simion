﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace BookStore.Models
{
    public class User
    {
        public string Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        [Display(Name="Password")]
        [DataType(DataType.Password)]
        public string PasswordHash { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        [RegularExpression("[0-9]{10}",ErrorMessage="Phone number must contain 10 digits")]
        public string PhoneNumber { get; set; }

        public IList<String> Roles { get; set; }


        public override string ToString()
        {
            return string.Format("Id={0} PasswordHash={1}", Id, (PasswordHash == null ? string.Empty : PasswordHash));
        }
    }
}